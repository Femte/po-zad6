#pragma once

#include "cuboid.hh"

/**
 * @brief Modeluje pojęcie klasy drona
 */
class drone : protected Cuboid {
private:

    /**
     * @brief Model wirnika
     */
    const std::string kMoverModel = "solid/MoverModel.dat";
    /**
    * @brief Wektor podstawowej translacji wirnika
    */
    Vector3D mover_translation{-17.5, -17.5, 20};
    /**
    * @brief Wektor podstawowej translacji drugiego wirnika
    */
    Vector3D mover2_translation{17.5, -17.5, 20};
    /**
    * @brief Wpsolrzedne wirnika
    */
    std::vector<Vector3D> mover_points;
    /**
    * @brief Wspolrzedne drugiego wirnika
    */
    std::vector<Vector3D> mover2_points;

protected:
    /**
     * @brief Obliczanie wektora translacji
     * @param MoveAngle - kat wznoszenia, opadania
     * @param length - odleglosc
     * @return Wektor przesuniecia z uwzglednionymi parametrami
     */
    [[nodiscard]] Vector3D CalculatingMove(double MoveAngle, double length) const;

public:
    /**
     * @brief Plik do zapisu wspolrzednych drona
     */
    const std::string kDroneFile = "solid/drone.dat";
    /**
     * @brief plik do zapisu wspolrzednych wirnika
     */

    const std::string kMoverFile = "solid/Mover.dat";
    /**
     * @brief plik do zapisu wspolrzednych drugiego wirnika
     */

    const std::string kMover2File = "solid/Mover2.dat";

    /**
     * @brief Rotacja drona wokol osi Z
     * @param AngleChange - kąt rotacji
     * @param FPS - liczba klatek w animacji
     */
    void Rotate(double AngleChange, const int &FPS);

    /**
    * @brief Rotacja wirnikow
    * @param FPS - liczba klatek w animacji
    */
    void RotateMovers(const int &FPS);
    /**
     * @brief Ruch drona wprzód
     * @param MovingAngle - kat wznoszenia, opadania
     * @param lenght - odleglosc
     * @param FPS - liczba klatek w animacji
     * @return - wspolrzedne oslony drona po wykonaniu ruchu
     */
    std::vector<Vector3D> TranslateForward(const double &MovingAngle, const double &lenght, const int &FPS);
    /**
     * @brief Konstruktor kklasy
     */
    drone();
    /**
     * @brief Destruktor klasy
     */
    ~drone() override;
    /**
     * @brief Zapisuje koordy drona do pliku
     * @param cuboid - plik drona
     * @param mover - plik wirnika
     * @param mover2 - plik drugiego wirnika
     */
    void draw(const std::string &cuboid, const std::string &mover, const std::string &mover2) const;
    /**
     * @brief Wyswietla informacje o kolzji, przy jednym dronie nie jest uzyteczna
     */
    void GetName() const override;

    [[nodiscard]] bool CheckColision(const std::vector<Vector3D> &colisionShield) const override { return false; }

};