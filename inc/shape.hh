#pragma once

#include <string>
#include "Vector.hh"
#include "Matrix.hh"

#define PI 3.14159265

/**
 * @brief Modeluje pojęcie klasy Obiektu Geometrycznego Eng: Shape
 */
class shape {

protected:
    /**
    * @brief Translacja obiektu geometrycznego
    */
    Vector3D translation;
    /**
    * @brief Rotacja obiektu geometrycznego
    */
    double angle;
public:
    /**
    * @brief Licznik wszystkich obiektów Vector3D
    */
    inline static unsigned allVectorCounter_ = 0;
    /**
    * @brief Licznik aktualnych obiektów Vector3D
    */
    inline static unsigned currentVectorCounter_ = 0;

    /**
     * @brief Zamiana stopni na radiany
     * @param degree - kat w stopniach
     * @return kat w radianach
     */
    [[nodiscard]] double DegreeToRadians(double degree) const { return (PI * degree / 100); }

    /**
     * @brief zmienia aktualny kat rotacji
     * @param InputAngle - kat o ktory chcemy zmienic rotacje
     */
    void AddAngleToFile(double &InputAngle) {
        angle += InputAngle;
    }

    /**
     * @brief Konstruktor klasy shape
     */
    shape() : angle{0} {
        allVectorCounter_++;
        currentVectorCounter_++;
    };

    /**
     * @brief Destruktor klasy shape
     */
    virtual ~shape() { currentVectorCounter_--; };

    /**
    * @brief Zmienia wektor translacji
    * @param change - zadany wektor translacji
    */
    void translate(const Vector3D &change) {
        translation = translation + change;
    }
    /**
     * @brief Zapis wspolrzednych do pliku
     * @param filename - nazwa pliku do ktorego zapisujemy punkty
     */
    virtual void draw(const std::string &filename) const = 0;

    /**
     * @brief informacje o kolizji
     */
    virtual void GetName() const = 0;

    /**
     * @brief Sprawdza kolizje
     * @param colisionShield - otoczka wokół drona
     * @return true - kolizja, false - poprawny ruch
     */
    [[nodiscard]] virtual bool CheckColision(const std::vector<Vector3D> &colisionShield) const = 0;
};