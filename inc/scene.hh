#pragma once

#include <memory>
#include "drone.hh"
#include "gnuplot_link.hh"
#include "fstream"
#include "string"
#include "BottomSurface.hh"
#include "UpperSurface.hh"
#include "Obstacles.hh"

/**
 * @brief Modeluje pojęcie klasy
 */
class scene {
private:
    /**
     * @brief Kontener przechowujący przeszkody
     */
    std::vector<std::shared_ptr<shape>> obstacles;
    /**
     * @brief Zmienna do obsługi gnuplota
     */
    PzG::GnuplotLink link;
    /**
     * @brief zmienna reprezentująca drona i jego elementy
     */
    drone drone_;
    /**
     * @brief Zmienna reprezentująca dno
     */

    BottomSurface bottomSurface;
    /**
     * @brief Zmienna reprezentująca taflę wody
     */
    UpperSurface upperSurface;
    /**
     * @brief Zmienna reprezentująca przeszkoda
     */

    CuboidObstacle cuboidObstacle;
    /**
     * @brief Zmienna reprezentująca przeszkoda
     */
    RodObstacle rodObstacle;
    /**
     * @brief Zmienna reprezentująca przeszkoda
     */
    RodObstacle2 rodObstacle2;
    /**
     * @brief Zmienna reprezentująca przeszkoda
     */
    CuboidObstacle2 cuboidObstacle2;
    /**
     * @brief Ilość klatek w animacji
     */
    const int FPS = 30;
    /**
     * @brief Przerwy pomiędzy animacjami
     */
    const int FREEZE = 16000;

public:
    /**
     * @brief Konstruktor klasy scena
     */
    scene();

    /**
     * @brief Destruktor klasy scena
     */

    ~scene() = default;

    /**
     * @brief Animacja obrotu drona
     * @param RotateAngle - kąt obrotu
     */

    void Rotation(double RotateAngle);

    /**
     * @brief Animacja ruchu
     * @param MovingAngle - kąt wznoszenia,opadania
     * @param lenght - odległość
     */

    void Move(const double &MovingAngle, const double &lenght);

    /**
     * @brief Sprawdza kolziję drona z obiektami sceny
     * @param colisionShield - współrzędne osłony drona
     * @return true - nastąpiła kolizja i wyświetlany jest komunikat, false - brak
     */

    bool CheckColision(const std::vector<Vector3D> &colisionShield);
    /**
     * @brief Wyświetlenie liczby obiektów Wektor3D
     */
    void ObjCounter();

};
