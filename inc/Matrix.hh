#pragma once

#include "Vector.hh"

using std::abs;

template<typename T, int SIZE>
class Matrix {
public:
    Vector<T, SIZE> row[SIZE];



    Matrix();

    const Vector<T, SIZE> &operator[](int column) const;

    Vector<T, SIZE> &operator[](int column);

    Vector<T, SIZE> operator*(Vector<T, SIZE> v); //mnozenie macierz razy wektor

    void transposed();

    Matrix<T, SIZE> operator*(T n) const; //mnozenie macierzy przez liczbe

    //void GaussElimination(Matrix<T, SIZE> &arg) const;

    //T Determinant(Matrix<T, SIZE> arg) const;


};



template<typename T, int SIZE>
Vector<T, SIZE> &Matrix<T, SIZE>::operator[](int column) {
    assert(column < SIZE);
    return row[column];
}

template<typename T, int SIZE>
const Vector<T, SIZE> &Matrix<T, SIZE>::operator[](int column) const {
    assert(column < SIZE);
    return row[column];
}

//funkcja transponowania macierzy
template<typename T, int SIZE>
void Matrix<T, SIZE>::transposed() {
    for (int i = 0; i < SIZE; i++) {
        for (int j = 0; j < SIZE; j++) {
            std::swap(row[i][j], row[j][i]);
        }
    }
}

template<typename T, int SIZE>
 Vector<T, SIZE> Matrix<T, SIZE>::operator*(Vector<T, SIZE> v) {
    Vector<T, SIZE> result;
    for(int j = 0 ; j < SIZE ; j++) {
        for (int i = 0; i < SIZE; ++i) {
            result[j] = result[j] + (row[j][i] * v[i]);
        }
    }
    return result;
}


template<typename T, int SIZE>
Matrix<T, SIZE> Matrix<T, SIZE>::operator*(T n) const  {
    for (int i = 0; i < SIZE; i++) {
        for (int j = 0; j < SIZE; j++) {
            row[i][j] = row[i][j] * n;
        }
    }
    return row;
}

template<typename T, int SIZE>
Matrix<T, SIZE>::Matrix() {
    for (int i = 0; i < SIZE; i++) {
        for (int j = 0; j < SIZE; j++) {
            row[i][j] = 0;
        }
    }

}

///**
// * Funckja wykonujaca algorytm Gaussa dla wybranej macierzy
// */
//template<typename T, int SIZE>
//void Matrix<T, SIZE>::GaussElimination(Matrix<T, SIZE> &arg) const {
//    for (int i = 0; i < SIZE - 1; i++) {
//        for (int k = i + 1; k < SIZE; k++) {
//            if (abs(arg[i][i] < EPS)) {//jezeli element glownej przekatnej = 0
//                arg[i] = arg[i] + arg[k];//dodaj do niego kolejny wiersz
//            }
//            T tmp = arg[k][i] / arg[i][i]; //zmienna sluzaca do zerowania wiersza
//            for (int j = 0; j < SIZE; j++) {
//                arg[k][j] = arg[k][j] - (arg[i][j] * tmp); //zerowanie wiersza
//            }
//        }
//    }
//}
//
///**
// * Funckja obliczajaca wyznacznik macierzy
// * @return wyznacznik
// */
//template<typename T, int SIZE>
//T Matrix<T, SIZE>::Determinant(Matrix<T, SIZE> arg) const {
//    T det;
//    GaussElimination(arg);
//    for (int i = 0; i < SIZE; i++) { //obliczanie wyznacznika
//        if (i == 0)
//            det = 1;
//
//        det = det * arg[i][i];
//    }
//    return det;
//}

/**
 * Ponizej znajduja sie przeciazenia operatorow << i >>
 * sluza one kolejno do wyswietlania oraz wczytywania
 * elementow macierzy
 */
template<typename T, int SIZE>
std::ostream &operator<<(std::ostream &os, const Matrix<T, SIZE> &Mac) {
    for (int j = 0; j < SIZE; j++) {
        for (int i = 0; i < SIZE; i++) {
            os << Mac[j][i] << "  ";
        }
        os << "\n";
    }
    return os;
}

template<typename T, int SIZE>
std::istream &operator>>(std::istream &is, Matrix<T, SIZE> &matrix) {
    while (isspace(is.peek())) //jezeli znakiem jest spacja to jest ignorowana
    {
        is.ignore();
    }
    for (int i = 0; i < SIZE; i++) {
        is >> matrix[i];
    }
    return is;
}

