#pragma once

#include <vector>
#include <string>
#include "Matrix.hh"
#include "RotateMatrix.hh"
#include "shape.hh"

    /**
     * @brief Modeluje pojecie klasy Cuboid
     */
class Cuboid : public shape {
protected:
    /**
     * @brief Kontener na wspolrzedne cuboida
     */
    std::vector<Vector3D> points;
    /**
     * @brief Kontener na wspolrzedne otoczki sprawdzajacej kolizje
     */
    std::vector<Vector3D> ColisionShieldPoints;
    /**
    * @brief Model cuboida
    */
    const std::string kModelCuboid = "solid/model.dat";


public:
    /**
    * @brief Konstruktor klasy cuboid
    */
    Cuboid();

    /**
    * @brief Destruktor klasy cuboid
    */

    ~Cuboid() override;

    /**
     * @brief Zapis wspolrzednych do pliku
     * @param filename - nazwa pliku do ktorego zapisujemy punkty
     */

    void draw(const std::string &filename) const override;

    /**
     * @brief Przelicza wspolrzedne oslony drona, na podstawie jego aktualnego polozenia
     * @param trans - wektor translacji
     * @return Zbior wspolrzednych oslony drona
     */

    [[nodiscard]] std::vector<Vector3D> ShieldDestinationCords(const Vector3D &trans) const;

    /**
     * @brief Zmiana polozenia oslony drona
     * @param movement - wektor przesuniecia drona
     */
    void ColisionShieldTranslation(const Vector3D &movement);

};


