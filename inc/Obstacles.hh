#include <string>
#include <vector>
#include "Matrix.hh"
#include "RotateMatrix.hh"
#include "shape.hh"

/**
 * @brief Modeluje pojęcie klasy
 */
class CuboidObstacle : public shape {
private:
    /**
     * @brief Zbiór współrzędnych przeszkody Cuboid
     */
    std::vector<Vector3D> CuboidObstaclePoints;

    /**
    * @brief Plik zawierający idealny model przeszkody
    */
    const std::string kModelCuboidObstacle = "solid/ModelCuboidObstacle.dat";
    /**
    * @brief Minimalne i maksymalne parametry wartości osi X
    *
    * Inizjalizowane z 0, żeby przypisać im wartości podczas pierwszego poboru danych
    */
    double min_X_ = 0.0 / 0.0, max_X_ = 0.0 / 0.0;
    /**
    * @brief Minimalne i maksymalne parametry wartości osi X
    *
    * Inizjalizowane z 0, żeby przypisać im wartości podczas pierwszego poboru danych
    */
    double min_Y_ = 0.0 / 0.0, max_Y_ = 0.0 / 0.0;
    /**
    * @brief Minimalne i maksymalne parametry wartości osi X
    *
    * Inizjalizowane z 0, żeby przypisać im wartości podczas pierwszego poboru danych
    */
    double min_Z_ = 0.0 / 0.0, max_Z_ = 0.0 / 0.0;

public:
    /**
     * @brief Konstruktor klasy
     */
    CuboidObstacle();

    /**
     * @brief Destruktor klasy
     */
    ~CuboidObstacle() override;

    /**
    * @brief Plik wynikowy ze współrzędnymi przeszkody Cuboid
    */

    const std::string kCuboidObstacle = "solid/CuboidObstacle.dat";

    /**
     * @brief Zapis współrzędnych do pliku
     * @param filename - plik wynikowy ze współrzędnymi
     */
    void draw(const std::string &filename) const override;

    /**
     * @brief Sprawdza kolizję danej przeszkody z dronem
     * @param colisionShield - współrzędne osłony drona
     * @return true - kolizja, false - brak
     */

    [[nodiscard]] bool CheckColision(const std::vector<Vector3D> &colisionShield) const override;

    /**
     * @brief Informacje o kolizji
     */

    void GetName() const override;

};

/**
 * @brief Modeluje pojęcie klasy
 */
class CuboidObstacle2 : public shape {
private:
    /**
     * @brief Zbiór współrzędnych przeszkody Cuboid
     */
    std::vector<Vector3D> CuboidObstaclePoints2;

    /**
    * @brief Plik zawierający idealny model przeszkody
    */
    const std::string kModelCuboidObstacle2 = "solid/ModelCuboidObstacle2.dat";
    /**
    * @brief Minimalne i maksymalne parametry wartości osi X
    *
    * Inizjalizowane z 0, żeby przypisać im wartości podczas pierwszego poboru danych
    */
    double min_X_ = 0.0 / 0.0, max_X_ = 0.0 / 0.0;
    /**
    * @brief Minimalne i maksymalne parametry wartości osi X
    *
    * Inizjalizowane z 0, żeby przypisać im wartości podczas pierwszego poboru danych
    */
    double min_Y_ = 0.0 / 0.0, max_Y_ = 0.0 / 0.0;
    /**
    * @brief Minimalne i maksymalne parametry wartości osi X
    *
    * Inizjalizowane z 0, żeby przypisać im wartości podczas pierwszego poboru danych
    */
    double min_Z_ = 0.0 / 0.0, max_Z_ = 0.0 / 0.0;

public:
    /**
     * @brief Konstruktor klasy
     */
    CuboidObstacle2();

    /**
     * @brief Destruktor klasy
     */
    ~CuboidObstacle2() override;

    /**
    * @brief Plik wynikowy ze współrzędnymi przeszkody Cuboid
    */

    const std::string kCuboidObstacle2 = "solid/CuboidObstacle2.dat";

    /**
     * @brief Zapis współrzędnych do pliku
     * @param filename - plik wynikowy ze współrzędnymi
     */
    void draw(const std::string &filename) const override;

    /**
     * @brief Sprawdza kolizję danej przeszkody z dronem
     * @param colisionShield - współrzędne osłony drona
     * @return true - kolizja, false - brak
     */

    [[nodiscard]] bool CheckColision(const std::vector<Vector3D> &colisionShield) const override;

    /**
     * @brief Informacje o kolizji
     */

    void GetName() const override;

};

/**
 * @brief Modeluje pojęcie klasy
 */
class RodObstacle : public shape {
private:
    /**
    * @brief Zbiór współrzędnych przeszkody Prętu
    */
    std::vector<Vector3D> RodObstaclePoints;
    /**
     * @brief Model pręta
     */
    const std::string kModelRodObstacle = "solid/ModelRodObstacle.dat";
    /**
     * @brief Bazowa translacja pręta
     */

    const Vector3D obstacleRodTranslation{-30, -90, 10};

    /**
     * @brief Minimalne i maksymalne parametry wartości osi X
     *
     * Inizjalizowane z 0, żeby przypisać im wartości podczas pierwszego poboru danych
     */
    double min_X_ = 0.0 / 0.0, max_X_ = 0.0 / 0.0;
    /**
    * @brief Minimalne i maksymalne parametry wartości osi X
    *
    * Inizjalizowane z 0, żeby przypisać im wartości podczas pierwszego poboru danych
    */
    double min_Y_ = 0.0 / 0.0, max_Y_ = 0.0 / 0.0;
    /**
    * @brief Minimalne i maksymalne parametry wartości osi X
    *
    * Inizjalizowane z 0, żeby przypisać im wartości podczas pierwszego poboru danych
    */
    double min_Z_ = 0.0 / 0.0, max_Z_ = 0.0 / 0.0;

public:
    /**
     * @brief Konstuktor klasy
     */
    RodObstacle();

    /**
     * @brief Destuktor klasy
     */
    ~RodObstacle() override;

    /**
     * @brief Plik ze wspołrzędnymi pręta
     */
    const std::string kRodObstacle = "solid/RodObstacle.dat";

    /**
     * @brief Zapis współrzędnych do pliku
     * @param filename - plik do ktorego zapisujemy współrzędne
     */

    void draw(const std::string &filename) const override;

    /**
     * @brief Sprawdzenie kolzji drona z prętem
     * @param colisionShield - współrzędne osłony drona
     * @return true - kolizja, false - brak
     */

    [[nodiscard]] bool CheckColision(const std::vector<Vector3D> &colisionShield) const override;

    /**
     * @brief Informacja o wystąpieniu kolizji
     */

    void GetName() const override;
};

/**
 * @brief Modeluje pojęcie klasy
 */
class RodObstacle2 : public shape {
private:
    /**
    * @brief Zbiór współrzędnych przeszkody Prętu
    */
    std::vector<Vector3D> RodObstaclePoints2;
    /**
     * @brief Model pręta
     */
    const std::string kModelRodObstacle2 = "solid/ModelRodObstacle2.dat";
    /**
     * @brief Bazowa translacja pręta
     */

    const Vector3D obstacleRodTranslation{-30, -90, 10};

    /**
     * @brief Minimalne i maksymalne parametry wartości osi X
     *
     * Inizjalizowane z 0, żeby przypisać im wartości podczas pierwszego poboru danych
     */
    double min_X_ = 0.0 / 0.0, max_X_ = 0.0 / 0.0;
    /**
    * @brief Minimalne i maksymalne parametry wartości osi X
    *
    * Inizjalizowane z 0, żeby przypisać im wartości podczas pierwszego poboru danych
    */
    double min_Y_ = 0.0 / 0.0, max_Y_ = 0.0 / 0.0;
    /**
    * @brief Minimalne i maksymalne parametry wartości osi X
    *
    * Inizjalizowane z 0, żeby przypisać im wartości podczas pierwszego poboru danych
    */
    double min_Z_ = 0.0 / 0.0, max_Z_ = 0.0 / 0.0;

public:
    /**
     * @brief Konstuktor klasy
     */
    RodObstacle2();

    /**
     * @brief Destuktor klasy
     */
    ~RodObstacle2() override;

    /**
     * @brief Plik ze wspołrzędnymi pręta
     */
    const std::string kRodObstacle2 = "solid/RodObstacle2.dat";

    /**
     * @brief Zapis współrzędnych do pliku
     * @param filename - plik do ktorego zapisujemy współrzędne
     */

    void draw(const std::string &filename) const override;

    /**
     * @brief Sprawdzenie kolzji drona z prętem
     * @param colisionShield - współrzędne osłony drona
     * @return true - kolizja, false - brak
     */

    [[nodiscard]] bool CheckColision(const std::vector<Vector3D> &colisionShield) const override;

    /**
     * @brief Informacja o wystąpieniu kolizji
     */

    void GetName() const override;
};