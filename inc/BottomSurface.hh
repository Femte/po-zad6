#pragma once

#include <vector>
#include "shape.hh"
#include <fstream>


/**
 * @brief Modeluje pojecie klasy podloża
 */
class BottomSurface : public shape {
protected:
    /**
     * @brief Model idealny podloża
     */
    const std::string kBottomModel = "solid/BottomSurfaceModel.dat";
    /**
     * @brief Zbior punktów podlożą
     */
    std::vector<Vector3D> BottomSurfacePoints;
public:
    /**
     * @brief Konstruktor klasy
     */
    BottomSurface();

    /**
     * @brief Destruktor klasy
     */

    ~BottomSurface() override;

    /**
     * @brief Plik do zapisu koordów podłoża
     */
    const std::string kBottomSurface = "solid/BottomSurface.dat";

    /**
     * @brief Zapis współrzędnych do pliku
     * @param filename - plik podłożą
     */

    void draw(const std::string &filename) const override;

    /**
     * @brief Wyświetla informację o kolizji drona z dnem
     */

    void GetName() const override;

    /**
     * @brief Poziom dna
     */

    double BottomSurfaceLevel = 0;
    /**
     * @brief Sprawdza kolizję drona z dnem
     * @param colisionShield - współrzędne osłony drona, ktora sprawdza kolizję
     * @return true - kolizja, false - brak kolizji
     */

    [[nodiscard]] bool CheckColision(const std::vector<Vector3D> &colisionShield) const override;
};