#pragma once

#include <vector>
#include "shape.hh"
#include <fstream>

/**
 * @brief Modeluje pojęcie klasy
 */
class UpperSurface : public shape {
protected:
    /**
     * @brief Model powierzchni tafli wody
     */
    const std::string kUpperModel = "solid/UpperSurfaceModel.dat";
    /**
     * @brief Zbiór współrzędnych tafli wody
     */
    std::vector<Vector3D> UpperSurfacePoints;
public:
    /**
     * @brief Konstruktor
     */
    UpperSurface();

    /**
     * @brief Destruktor
     */

    ~UpperSurface() override;

    /**
     * @brief Plik ze współrzędnymi tafli wody
     */

    const std::string kUpperSurface = "solid/UpperSurface.dat";

    /**
     * @brief Zapis współrzędnych tafli wody do pliku
     * @param filename - plik wynikowy z zapisanymi współrzędnymi
     */

    void draw(const std::string &filename) const override;

    /**
     * @brief Informacja o wynurzeniu
     */

    void GetName() const override;

    /**
     * @brief Górny próg mapy
     */

    double UpperSurfaceLevel = 335;

    /**
     * @brief Sprawdza wynurzenie
     * @param colisionShield - współrzędne osłony drona
     * @return true - wynurzenie, false - brak
     */

    [[nodiscard]] bool CheckColision(const std::vector<Vector3D> &colisionShield) const override;

};