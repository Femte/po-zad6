#include "cuboid.hh"
#include <fstream>
#include <iostream>
#include <thread>


using namespace std;

Cuboid::Cuboid() {
    ifstream inputFile;
    inputFile.open(kModelCuboid);
    if (!inputFile.is_open()) {
        cerr << "Unable to load model Cuboid file!"
             << endl;
        return;
    }

    Vector3D point;
    while (inputFile >> point) {
        points.push_back(point);
        allVectorCounter_++;
        currentVectorCounter_++;
        ColisionShieldPoints.push_back(point * 1.2); //stworzenie tarczy ktora osłania cuboid z kazdej strony
    }
    inputFile.close();
}

void Cuboid::draw(const std::string &filename) const {
    ofstream outputFile;
    RotateMatrix m;
    outputFile.open(filename);
    if (!outputFile.is_open()) {
        cerr << "Unable to open drone file!" << endl;
        return;
    }
    for (unsigned i = 0; i < points.size(); ++i) {
        outputFile <<  m.RotateZ(angle) * points[i] + translation << endl;
        if (i % 4 == 3) // triggers after every 4 points
        {
            outputFile << "#\n\n";
        }
    }
}

vector<Vector3D> Cuboid::ShieldDestinationCords(const Vector3D &trans) const {
    vector<Vector3D> DestinationShieldPoints;

    DestinationShieldPoints.reserve(ColisionShieldPoints.size()); //rezerwacja pamieci

    for (const auto &ColisionShieldPoint : ColisionShieldPoints) { //wypelnienie puntkow polozenia po translacji
        DestinationShieldPoints.push_back(ColisionShieldPoint + trans);
    }

    return DestinationShieldPoints;

}

void Cuboid::ColisionShieldTranslation(const Vector3D &movement) {
    for (auto & ColisionShieldPoint : ColisionShieldPoints) { //kordy po translacji
        ColisionShieldPoint = ColisionShieldPoint + movement;
    }
}

Cuboid::~Cuboid() {
    currentVectorCounter_ -= points.size();
}

