#include "scene.hh"
#include <chrono>
#include <thread>

using namespace std::chrono;
using namespace std::this_thread;
using namespace std;

scene::scene() {

    link.Init();
    link.SetDrawingMode(PzG::DM_3D);

    link.SetRangeX(-300, 300);
    link.SetRangeY(-300, 300);
    link.SetRangeZ(-300, 300);

//    bottomSurface.draw(bottomSurface.kBottomSurface);
    link.AddFilename(bottomSurface.kBottomSurface.c_str(), PzG::LS_CONTINUOUS, 1);

//    upperSurface.draw(upperSurface.kUpperSurface);
    link.AddFilename(upperSurface.kUpperSurface.c_str(), PzG::LS_CONTINUOUS, 1);

    cuboidObstacle.draw(cuboidObstacle.kCuboidObstacle);
    link.AddFilename(cuboidObstacle.kCuboidObstacle.c_str(), PzG::LS_CONTINUOUS, 1);

//    cuboidObstacle2.draw(cuboidObstacle2.kCuboidObstacle2);
    link.AddFilename(cuboidObstacle2.kCuboidObstacle2.c_str(), PzG::LS_CONTINUOUS, 1);

//    rodObstacle.draw(rodObstacle.kRodObstacle);
    link.AddFilename(rodObstacle.kRodObstacle.c_str(), PzG::LS_CONTINUOUS, 1);

//    rodObstacle2.draw(rodObstacle2.kRodObstacle2);
    link.AddFilename(rodObstacle2.kRodObstacle2.c_str(), PzG::LS_CONTINUOUS, 1);

    drone_.draw(drone_.kDroneFile, drone_.kMoverFile, drone_.kMover2File);
    link.AddFilename(drone_.kMover2File.c_str(), PzG::LS_CONTINUOUS, 1);
    link.AddFilename(drone_.kMoverFile.c_str(), PzG::LS_CONTINUOUS, 1);
    link.AddFilename(drone_.kDroneFile.c_str(), PzG::LS_CONTINUOUS, 1);


    link.Draw();
}

void scene::Move(const double &MovingAngle, const double &lenght) { //animacja ruchu
    for (unsigned i = 0; i < FPS; ++i) {
        if (CheckColision(drone_.TranslateForward(MovingAngle, lenght, FPS)))
            break;
        link.Draw();
        sleep_for(microseconds(FREEZE));
    }
}



void scene::Rotation(double RotateAngle) { //animacja obrotu
    for (unsigned i = 0; i < FPS; ++i) {
        drone_.Rotate(RotateAngle, FPS);
        link.Draw();
        sleep_for(microseconds(FREEZE));
    }
}

bool scene::CheckColision(const std::vector<Vector3D> &colisionShield) {

    //wypelnienie kontenera przeszkodami
    obstacles.push_back(make_shared<BottomSurface>());
    obstacles.push_back(make_shared<UpperSurface>());
    obstacles.push_back(make_shared<CuboidObstacle>());
    obstacles.push_back(make_shared<RodObstacle>());
    obstacles.push_back(make_shared<RodObstacle2>());
    obstacles.push_back(make_shared<CuboidObstacle2>());

    for (const auto &obstacle : obstacles) {         // Sprawdzanie kolizji powłoki
        if (obstacle->CheckColision(colisionShield)) {// drona z obiektami na scenie
            obstacle->GetName();  // Komunikat o kolizji
            return true;
        }

    }
    return false;
}

void scene::ObjCounter() {
    cout << "Aktualna ilosc obiektow Vector3D: " << shape::currentVectorCounter_ << endl;

    cout << "  Laczna ilosc obiektow Vector3D: " << shape::allVectorCounter_ << endl << "\n";

}


