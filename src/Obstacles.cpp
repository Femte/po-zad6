#include "Obstacles.hh"
#include <fstream>

using namespace std;

void CuboidObstacle::draw(const std::string &filename) const {
    ofstream outputFile;
    outputFile.open(filename);    // Otwiera plik do zapisu koordynatów
    if (!outputFile.is_open()) {    // Kontrola poprawności wczytanego pliku
        cerr << "Error: Nie mozna wczytac pliku przeszkody cuboida!" << endl;
        return;
    }

    for (unsigned i = 0; i < CuboidObstaclePoints.size(); ++i) {// Zapis koordynatów
        outputFile << CuboidObstaclePoints[i] << endl;
        if (i % 4 == 3) {
            outputFile << "#\n\n";
        }
    }
}

CuboidObstacle::CuboidObstacle() {
    ifstream inputFile;
    inputFile.open(kModelCuboidObstacle);
    if (!inputFile.is_open()) {
        cerr << "Unable to load model cuboid obstacle file!"
             << endl;
        return;
    }

    Vector3D point;
    while (inputFile >> point) {
        CuboidObstaclePoints.push_back(point);
        allVectorCounter_++;    // Zwiększa liczbę wszystkich obiektów Vector3D
        currentVectorCounter_++;    // Zwiększa aktualną liczbę obj. Vector3D
        if (!(point[0] >= min_X_)) min_X_ = point[0];  // Minimalna wartość OX
        if (!(point[0] <= max_X_)) max_X_ = point[0];  // Maksymalna wartość OX

        if (!(point[1] >= min_Y_)) min_Y_ = point[1];  // Minimalna wartość OY
        if (!(point[1] <= max_Y_)) max_Y_ = point[1];  // Maksymalna wartość OY

        if (!(point[2] >= min_Z_)) min_Z_ = point[2];  // Minimalna wartość OZ
        if (!(point[2] <= max_Z_)) max_Z_ = point[2];  // Maksymalna wartość OZ
    }
    inputFile.close();
}

bool CuboidObstacle::CheckColision(const std::vector<Vector3D> &colisionShield) const {
    // Maksymalne i minimalne wartości koordynatów osłony drona zainicjowane
    // wartościami NaN, aby przypisać im wartość przy pierwszym przejściu pętli
    double coverXMin = 0.0 / 0.0, coverXMax = 0.0 / 0.0;
    double coverYMin = 0.0 / 0.0, coverYMax = 0.0 / 0.0;
    double coverZMin = 0.0 / 0.0, coverZMax = 0.0 / 0.0;

    for (int i = 1; i < colisionShield.size(); ++i) {
        // Obliczanie minimalnej wartości osi X dla osłony drona
        if (!(colisionShield[i][0] >= coverXMin)) coverXMin = colisionShield[i][0];
        // Obliczanie maksymalnej wartości osi X dla osłony drona
        if (!(colisionShield[i][0] <= coverXMax)) coverXMax = colisionShield[i][0];
        // Obliczanie minimalnej wartości osi Y dla osłony drona
        if (!(colisionShield[i][1] >= coverYMin)) coverYMin = colisionShield[i][1];
        // Obliczanie maksymalnej wartości osi Y dla osłony drona
        if (!(colisionShield[i][1] <= coverYMax)) coverYMax = colisionShield[i][1];
        // Obliczanie minimalnej wartości osi Z dla osłony drona
        if (!(colisionShield[i][2] >= coverZMin)) coverZMin = colisionShield[i][2];
        // Obliczanie maksymalnej wartości osi Z dla osłony drona
        if (!(colisionShield[i][2] <= coverZMax)) coverZMax = colisionShield[i][2];
    }

    // Zwracanie współrzędnych kolizyjności osłony
    return (coverXMin <= max_X_ && coverXMax >= min_X_) && (coverYMin <= max_Y_ && coverYMax >= min_Y_) &&
           (coverZMin <= max_Z_ && coverZMax >= min_Z_);
}

void CuboidObstacle::GetName() const {
    cerr << "Dron uderzyl w prostokatna przeszkode" << endl;
}

CuboidObstacle::~CuboidObstacle() {
    currentVectorCounter_ -= CuboidObstaclePoints.size();    // Zmniejsza liczbe
    // aktualnych obiektów typu Vector3D
}


RodObstacle::RodObstacle() {
    ifstream inputFile;
    inputFile.open(kModelRodObstacle);
    if (!inputFile.is_open()) {
        cerr << "Unable to load model cuboid obstacle file!"
             << endl;
        return;
    }

    Vector3D point;
    while (inputFile >> point) {
        point = point + obstacleRodTranslation;
        RodObstaclePoints.push_back(point);
        allVectorCounter_++;    // Zwiększa liczbę wszystkich obiektów Vector3D
        currentVectorCounter_++;    // Zwiększa aktualną liczbę obj. Vector3D

        if (!(point[0] >= min_X_)) min_X_ = point[0];  // Minimalna wartość OX
        if (!(point[0] <= max_X_)) max_X_ = point[0];  // Maksymalna wartość OX

        if (!(point[1] >= min_Y_)) min_Y_ = point[1];  // Minimalna wartość OX
        if (!(point[1] <= max_Y_)) max_Y_ = point[1];  // Maksymalna wartość OX

        if (!(point[2] >= min_Z_)) min_Z_ = point[2];  // Minimalna wartość OX
        if (!(point[2] <= max_Z_)) max_Z_ = point[2];  // Maksymalna wartość OX
    }
    inputFile.close();
}

void RodObstacle::draw(const std::string &filename) const {
    ofstream outputFile;
    outputFile.open(filename);   // Otwiera plik do zapisu koordynatów
    if (!outputFile.is_open()) {    // Kontrola poprawności wczytanego pliku
        cerr << "Error: Nie mozna wczytac pliku preta!" << endl;
        return;
    }

    for (unsigned i = 0; i < RodObstaclePoints.size(); ++i) {   // Zapis koordynatów
        outputFile << RodObstaclePoints[i] << endl;
        if (i % 4 == 3) {
            outputFile << "#\n\n";
        }
    }
}

bool RodObstacle::CheckColision(const std::vector<Vector3D> &colisionShield) const {
    // Maksymalne i minimalne wartości koordynatów osłony drona zainicjowane
    // wartościami NaN, aby przypisać im wartość przy pierwszym przejściu pętli
    double coverXMin = 0.0 / 0.0, coverXMax = 0.0 / 0.0;
    double coverYMin = 0.0 / 0.0, coverYMax = 0.0 / 0.0;
    double coverZMin = 0.0 / 0.0, coverZMax = 0.0 / 0.0;

    for (int i = 1; i < colisionShield.size(); ++i) {
        // Obliczanie minimalnej wartości osi X dla osłony drona
        if (!(colisionShield[i][0] >= coverXMin)) coverXMin = colisionShield[i][0];
        // Obliczanie maksymalnej wartości osi X dla osłony drona
        if (!(colisionShield[i][0] <= coverXMax)) coverXMax = colisionShield[i][0];
        // Obliczanie minimalnej wartości osi Y dla osłony drona
        if (!(colisionShield[i][1] >= coverYMin)) coverYMin = colisionShield[i][1];
        // Obliczanie maksymalnej wartości osi Y dla osłony drona
        if (!(colisionShield[i][1] <= coverYMax)) coverYMax = colisionShield[i][1];
        // Obliczanie minimalnej wartości osi Z dla osłony drona
        if (!(colisionShield[i][2] >= coverZMin)) coverZMin = colisionShield[i][2];
        // Obliczanie maksymalnej wartości osi Z dla osłony drona
        if (!(colisionShield[i][2] <= coverZMax)) coverZMax = colisionShield[i][2];
    }

    // Zwracanie współrzędnych kolizyjności osłony
    return (coverXMin <= max_X_ && coverXMax >= min_X_) && (coverYMin <= max_Y_
                                                            && coverYMax >= min_Y_) &&
           (coverZMin <= max_Z_ && coverZMax >= min_Z_);;
}


RodObstacle::~RodObstacle() {
    currentVectorCounter_ -= RodObstaclePoints.size();   // Zmniejsza liczbę
    // aktualnych obiektów typu Vector3D
}

void RodObstacle::GetName() const {
    cerr << endl << "Zatrzymano drona przed zderzeniem z przeszkoda "
                    "Preta!" << endl;
}

RodObstacle2::RodObstacle2() {
    ifstream inputFile;
    inputFile.open(kModelRodObstacle2);
    if (!inputFile.is_open()) {
        cerr << "Unable to load model cuboid obstacle file!"
             << endl;
        return;
    }

    Vector3D point;
    while (inputFile >> point) {
        point = point + obstacleRodTranslation;
        RodObstaclePoints2.push_back(point);
        allVectorCounter_++;    // Zwiększa liczbę wszystkich obiektów Vector3D
        currentVectorCounter_++;    // Zwiększa aktualną liczbę obj. Vector3D

        if (!(point[0] >= min_X_)) min_X_ = point[0];  // Minimalna wartość OX
        if (!(point[0] <= max_X_)) max_X_ = point[0];  // Maksymalna wartość OX

        if (!(point[1] >= min_Y_)) min_Y_ = point[1];  // Minimalna wartość OX
        if (!(point[1] <= max_Y_)) max_Y_ = point[1];  // Maksymalna wartość OX

        if (!(point[2] >= min_Z_)) min_Z_ = point[2];  // Minimalna wartość OX
        if (!(point[2] <= max_Z_)) max_Z_ = point[2];  // Maksymalna wartość OX
    }
    inputFile.close();
}

RodObstacle2::~RodObstacle2() {
    currentVectorCounter_ -= RodObstaclePoints2.size();   // Zmniejsza liczbę
    // aktualnych obiektów typu Vector3D
}

void RodObstacle2::draw(const std::string &filename) const {
    ofstream outputFile;
    outputFile.open(filename);   // Otwiera plik do zapisu koordynatów
    if (!outputFile.is_open()) {    // Kontrola poprawności wczytanego pliku
        cerr << "Error: Nie mozna wczytac pliku preta!" << endl;
        return;
    }

    for (unsigned i = 0; i < RodObstaclePoints2.size(); ++i) {   // Zapis koordynatów
        outputFile << RodObstaclePoints2[i] << endl;
        if (i % 4 == 3) {
            outputFile << "#\n\n";
        }
    }
}

bool RodObstacle2::CheckColision(const std::vector<Vector3D> &colisionShield) const {
    // Maksymalne i minimalne wartości koordynatów osłony drona zainicjowane
    // wartościami NaN, aby przypisać im wartość przy pierwszym przejściu pętli
    double coverXMin = 0.0 / 0.0, coverXMax = 0.0 / 0.0;
    double coverYMin = 0.0 / 0.0, coverYMax = 0.0 / 0.0;
    double coverZMin = 0.0 / 0.0, coverZMax = 0.0 / 0.0;

    for (int i = 1; i < colisionShield.size(); ++i) {
        // Obliczanie minimalnej wartości osi X dla osłony drona
        if (!(colisionShield[i][0] >= coverXMin)) coverXMin = colisionShield[i][0];
        // Obliczanie maksymalnej wartości osi X dla osłony drona
        if (!(colisionShield[i][0] <= coverXMax)) coverXMax = colisionShield[i][0];
        // Obliczanie minimalnej wartości osi Y dla osłony drona
        if (!(colisionShield[i][1] >= coverYMin)) coverYMin = colisionShield[i][1];
        // Obliczanie maksymalnej wartości osi Y dla osłony drona
        if (!(colisionShield[i][1] <= coverYMax)) coverYMax = colisionShield[i][1];
        // Obliczanie minimalnej wartości osi Z dla osłony drona
        if (!(colisionShield[i][2] >= coverZMin)) coverZMin = colisionShield[i][2];
        // Obliczanie maksymalnej wartości osi Z dla osłony drona
        if (!(colisionShield[i][2] <= coverZMax)) coverZMax = colisionShield[i][2];
    }

    // Zwracanie współrzędnych kolizyjności osłony
    return (coverXMin <= max_X_ && coverXMax >= min_X_) && (coverYMin <= max_Y_
                                                            && coverYMax >= min_Y_) &&
           (coverZMin <= max_Z_ && coverZMax >= min_Z_);;
}

void RodObstacle2::GetName() const {
    cerr << endl << "Zatrzymano drona przed zderzeniem z pretem!" << endl;
}

CuboidObstacle2::CuboidObstacle2() {
    ifstream inputFile;
    inputFile.open(kModelCuboidObstacle2);
    if (!inputFile.is_open()) {
        cerr << "Unable to load model cuboid obstacle file!"
             << endl;
        return;
    }

    Vector3D point;
    while (inputFile >> point) {
        CuboidObstaclePoints2.push_back(point);
        allVectorCounter_++;    // Zwiększa liczbę wszystkich obiektów Vector3D
        currentVectorCounter_++;    // Zwiększa aktualną liczbę obj. Vector3D
        if (!(point[0] >= min_X_)) min_X_ = point[0];  // Minimalna wartość OX
        if (!(point[0] <= max_X_)) max_X_ = point[0];  // Maksymalna wartość OX

        if (!(point[1] >= min_Y_)) min_Y_ = point[1];  // Minimalna wartość OY
        if (!(point[1] <= max_Y_)) max_Y_ = point[1];  // Maksymalna wartość OY

        if (!(point[2] >= min_Z_)) min_Z_ = point[2];  // Minimalna wartość OZ
        if (!(point[2] <= max_Z_)) max_Z_ = point[2];  // Maksymalna wartość OZ
    }
    inputFile.close();
}

CuboidObstacle2::~CuboidObstacle2() {
    currentVectorCounter_ -= CuboidObstaclePoints2.size();    // Zmniejsza liczbe
    // aktualnych obiektów typu Vector3D
}

void CuboidObstacle2::draw(const std::string &filename) const {
    ofstream outputFile;
    outputFile.open(filename);    // Otwiera plik do zapisu koordynatów
    if (!outputFile.is_open()) {    // Kontrola poprawności wczytanego pliku
        cerr << "Error: Nie mozna wczytac pliku przeszkody cuboida!" << endl;
        return;
    }

    for (unsigned i = 0; i < CuboidObstaclePoints2.size(); ++i) {// Zapis koordynatów
        outputFile << CuboidObstaclePoints2[i] << endl;
        if (i % 4 == 3) {
            outputFile << "#\n\n";
        }
    }
}

void CuboidObstacle2::GetName() const {
    cerr << "Dron uderzyl w prostokatna przeszkode!" << endl;
}

bool CuboidObstacle2::CheckColision(const std::vector<Vector3D> &colisionShield) const {
    // Maksymalne i minimalne wartości koordynatów osłony drona zainicjowane
    // wartościami NaN, aby przypisać im wartość przy pierwszym przejściu pętli
    double coverXMin = 0.0 / 0.0, coverXMax = 0.0 / 0.0;
    double coverYMin = 0.0 / 0.0, coverYMax = 0.0 / 0.0;
    double coverZMin = 0.0 / 0.0, coverZMax = 0.0 / 0.0;

    for (int i = 1; i < colisionShield.size(); ++i) {
        // Obliczanie minimalnej wartości osi X dla osłony drona
        if (!(colisionShield[i][0] >= coverXMin)) coverXMin = colisionShield[i][0];
        // Obliczanie maksymalnej wartości osi X dla osłony drona
        if (!(colisionShield[i][0] <= coverXMax)) coverXMax = colisionShield[i][0];
        // Obliczanie minimalnej wartości osi Y dla osłony drona
        if (!(colisionShield[i][1] >= coverYMin)) coverYMin = colisionShield[i][1];
        // Obliczanie maksymalnej wartości osi Y dla osłony drona
        if (!(colisionShield[i][1] <= coverYMax)) coverYMax = colisionShield[i][1];
        // Obliczanie minimalnej wartości osi Z dla osłony drona
        if (!(colisionShield[i][2] >= coverZMin)) coverZMin = colisionShield[i][2];
        // Obliczanie maksymalnej wartości osi Z dla osłony drona
        if (!(colisionShield[i][2] <= coverZMax)) coverZMax = colisionShield[i][2];
    }

    // Zwracanie współrzędnych kolizyjności osłony
    return (coverXMin <= max_X_ && coverXMax >= min_X_) && (coverYMin <= max_Y_ && coverYMax >= min_Y_) &&
           (coverZMin <= max_Z_ && coverZMax >= min_Z_);
}
