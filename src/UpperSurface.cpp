#include "UpperSurface.hh"
using namespace std;

UpperSurface::UpperSurface() {
    ifstream inputFile;
    inputFile.open(kUpperModel);
    if (!inputFile.is_open()) {
        cerr << "Unable to load upper surface file!"
             << endl;
        return;
    }

    Vector3D point;
    while (inputFile >> point) {
        UpperSurfacePoints.push_back(point);
        allVectorCounter_++;    // Zwiększa liczbę wszystkich obiektów Vector3D
        currentVectorCounter_++;    // Zwiększa aktualną liczbę obj. Vector3D
    }
    inputFile.close();
}

void UpperSurface::draw(const std::string &filename) const {
    ofstream outputFile;
    outputFile.open(filename);
    if (!outputFile.is_open()) {
        cerr << "Unable to bottom surface file!" << endl;
        return;
    }
    for (unsigned i = 0; i < UpperSurfacePoints.size(); ++i) {
        outputFile << UpperSurfacePoints[i]<< endl;
        if (i % 4 == 3) // triggers after every 4 points
        {
            outputFile << "#\n\n";
        }
    }
}

void UpperSurface::GetName() const {
    cerr << endl << "Zatrzymano drona przed nadmiernym wynurzeniem!"
         << endl;
}

bool UpperSurface::CheckColision(const std::vector<Vector3D> &colisionShield) const {
//    for (auto &cord : colisionShield) {
//        if (cord[2] >= UpperSurfaceLevel)
//            return true;
//    }
//    return false;
    // Maksymalne i minimalne wartości koordynatów osłony drona zainicjowane
    // wartościami NaN, aby przypisać im wartość przy pierwszym przejściu pętli
    double coverZMin = 0.0 / 0.0, coverZMax = 0.0 / 0.0;

    // Szuka największą i najmniejszą wartość współrzędnej osi OZ
    for (int i = 1; i < colisionShield.size(); ++i) {
        if (!(coverZMin <= colisionShield[i][2])) coverZMin = colisionShield[i][2];
        if (!(coverZMax >= colisionShield[i][2])) coverZMax = colisionShield[i][2];
    }

    double centerDrone = 0.5 * (coverZMin + coverZMax);

    return centerDrone > UpperSurfaceLevel;
}

UpperSurface::~UpperSurface() {
    currentVectorCounter_ -= UpperSurfacePoints.size(); // Zmniejsza liczbe aktualnych
    // obiektów typu Vector3D
}

