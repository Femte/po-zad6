#include "drone.hh"
#include <fstream>

using namespace std;


void drone::Rotate(double AngleChange, const int &FPS) {
    AngleChange /= FPS;
    Cuboid::AddAngleToFile(AngleChange);
    RotateMovers(FPS);
    draw(kDroneFile, kMoverFile, kMover2File);
}


void drone::draw(const std::string &cuboid, const std::string &mover, const std::string &mover2) const {
    Cuboid::draw(cuboid);
    RotateMatrix n;
    ofstream outputFile;
    outputFile.open(mover);
    if (!outputFile.is_open()) {
        cerr << "Unable to open mover file!" << endl;
        return;
    }
    for (unsigned i = 0; i < mover_points.size(); ++i) {
        outputFile << (n.RotateZ(angle) * (mover_points[i] + mover_translation)) + translation << endl;
        if (i % 4 == 3) // triggers after every 4 points
        {
            outputFile << "#\n\n";
        }
    }
    ofstream outputFile2;
    outputFile2.open(mover2);
    if (!outputFile2.is_open()) {
        cerr << "Unable to open mover file!" << endl;
        return;
    }
    for (unsigned i = 0; i < mover2_points.size(); ++i) {
        outputFile2 << (n.RotateZ(angle) * (mover2_points[i] + mover2_translation)) + translation << endl;
        if (i % 4 == 3) // triggers after every 4 points
        {
            outputFile2 << "#\n\n";
        }
    }

    outputFile.close();
    outputFile2.close();

}

vector<Vector3D> drone::TranslateForward(const double &MovingAngle, const double &lenght, const int &FPS) {
    Vector3D translation = (CalculatingMove(MovingAngle, lenght)/FPS);
    Cuboid::translate(translation);
    Cuboid::ColisionShieldTranslation(translation);
    RotateMovers(FPS);
    draw(kDroneFile, kMoverFile, kMover2File);
    return Cuboid::ShieldDestinationCords(translation);
}


Vector3D drone::CalculatingMove(double MoveAngle, double length) const {
    Vector3D result;
    double param1 = PI * MoveAngle / 100;
    double param2 = PI * angle / 100;

    if (MoveAngle == 90) {
        result[0] = 0;
        result[1] = 0;
        result[2] = length;
    } else if (MoveAngle == -90) {
        result[0] = 0;
        result[1] = 0;
        result[2] = -length;
    } else {
        while (MoveAngle > 360)
            MoveAngle = MoveAngle - 360;

        result[0] = cos(param1) * cos(param2) * length;
        result[1] = cos(param1) * sin(param2) * length;
        result[2] = sin(param1) * length;
    }

    return result;
}


void drone::RotateMovers(const int &FPS) {
    RotateMatrix m;

    for (auto &mover_point : mover_points) {
        mover_point = m.RotateZ(360 / FPS) * mover_point;
    }
    for (auto &mover2_point : mover2_points) {
        mover2_point = m.RotateZ(360 / FPS) * mover2_point;
    }

}


drone::drone() {
    ifstream inputFile;

    inputFile.open(kMoverModel);
    if (!inputFile.is_open()) {
        cerr << "Unable to load model mover file!"
             << endl;
        return;
    }

    Vector3D point;
    while (inputFile >> point) {
        mover_points.push_back(point);
        mover2_points.push_back(point);
        currentVectorCounter_ += 2; // Zwiększa o 2 liczbę obiektów Vector3D
        allVectorCounter_ += 2; // Zwiększa o 2 liczbę akt. obiektów Vector3D
    }
    inputFile.close();

}

void drone::GetName() const {
    cerr << "cos tam" << endl;
}

drone::~drone() { //zmniejsza liczbe Wektorow3D
    currentVectorCounter_ -= (mover_points.size() + mover2_points.size());
}



