#include "BottomSurface.hh"

using namespace std;

BottomSurface::BottomSurface() {
    ifstream inputFile;
    inputFile.open(kBottomModel);
    if (!inputFile.is_open()) {
        cerr << "Unable to load bottom surface file!"
             << endl;
        return;
    }

    Vector3D point;
    while (inputFile >> point) {
        BottomSurfacePoints.push_back(point);
        allVectorCounter_++;    // Zwiększa liczbę wszystkich obiektów Vector3D
        currentVectorCounter_++;    // Zwiększa aktualną liczbę obj. Vector3D
    }
    inputFile.close();
}

void BottomSurface::draw(const std::string &filename) const {
    ofstream outputFile;
    outputFile.open(filename);
    if (!outputFile.is_open()) {
        cerr << "Unable to bottom surface file!" << endl;
        return;
    }
    for (unsigned i = 0; i < BottomSurfacePoints.size(); ++i) {
        outputFile << BottomSurfacePoints[i] << endl;
        if (i % 4 == 3) // triggers after every 4 points
        {
            outputFile << "#\n\n";
        }
    }
}

void BottomSurface::GetName() const {
    cerr << endl << "Komunikat: Zatrzymano drona przed rozbiciem o "
                    "powierzchnie dna!" << endl;
}

bool BottomSurface::CheckColision(const std::vector<Vector3D> &colisionShield) const {
    for (auto &cord : colisionShield) {
        if (cord[2] <= BottomSurfaceLevel) {
            cerr << "Dron uderzyl w dno" << endl;
            return true;
        }
    }
    return false;
}

BottomSurface::~BottomSurface() {
    currentVectorCounter_ -= BottomSurfacePoints.size(); // Zmniejsza liczbe aktualnych
    // obiektów typu Vector3D
}
